import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;

import java.io.FileWriter;
import java.io.IOException;


public class RDFExample2 extends Object {

    public static void main(String[] args) throws IOException {
        Model model = ModelFactory.createDefaultModel();

        model.read("https://www.w3.org/TR/2002/WD-rdf-schema-20020430/rdfs-namespace.xml", null, "RDF/XML");
        FileWriter writer = new FileWriter("results/example-2.ttl");
        model.write(writer, "TURTLE");
    }
}
