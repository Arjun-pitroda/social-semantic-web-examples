from SPARQLWrapper import SPARQLWrapper, TURTLE
from rdflib import Graph

# Describe Query
sparql = SPARQLWrapper("http://dbpedia.org/sparql")
sparql.setQuery("""
    DESCRIBE <http://dbpedia.org/resource/Turkey>
""")

sparql.setReturnFormat(TURTLE)
results = sparql.query().convert()

g = Graph()
g.parse(data=results, format="turtle")

with open("describe-results.ttl", "wb") as f:
    f.write(g.serialize(format='turtle'))
